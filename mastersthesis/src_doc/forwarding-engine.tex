\section{Forwarding Decision Algorithm}
\label{forwarding-decision-algorithm}

\noindent Implemented forwarding decision algorithm is consistent with the~IEEE~802.1Q~standard and its theoretical background has been presented in section~\ref{input-flow}. The details of the~algorithm impacts overall forwarding engine architecture. The forwarding decision flowchart is depicted in figure \ref{fig:forwarding-decision} and its description is contained below.

\ \\
\noindent \textbf{Initial configuration verification}
\begin{enumerate}
	\item The~data set of the~forwarding engine request is specified in \ref{t:request}.
	\item Forwarding engine configuration, specified in \ref{t:global-params} and \ref{t:port-params}, is verified. Request is processed if global enable for forwarding engine is set and if request port is configured in either \textit{pass~all} or \textit{pass~local~link} mode. Otherwise the~frame associated with the~request is supposed to be dropped.
\end{enumerate}

\noindent \textbf{VLAN configuration verification}
\begin{enumerate}[resume]
	\item The VLAN table entry for VLAN~ID from the~request is read.
	\item If the~VLAN~ID from the~request is regarded as illegal according to the~configuration kept in the~VLAN table, the~frame associated with the~request is supposed to be dropped.
\end{enumerate}

\noindent \textbf{Configuration for source MAC address verification}
\begin{enumerate}[resume]
	\item The HASH table entry for source MAC address from the~request and the~Filtering~ID read previously from the~VLAN~table is searched.
	\item If the~entry is not found in the~HASH~table, the~source MAC address from the~request and its associated source port ID are supposed to be written to the~HASH~table within a~\textit{learning process}, which is described in section~\ref{transparent-bridges}. For this purpose, the~data set specified in \ref{t:learning-fifo} is written to the appropriate buffer, provided that learning process for the specific port ID is enabled and there is a place in the buffer. The buffer is read by the~CPU, which manages the contents of the~HASH~table. 
	\item If the~entry is found and has the~\textit{drop when source} flag set, the~frame associated with the~request is supposed to be dropped.
	\item The \textit{port source mask} from the~HASH~table entry is compared with the source port ID. In case of an inconsistency and the \textit{drop unmatched} flag from the~HASH~table entry set, the~frame associated with the~request is supposed to be dropped.
\end{enumerate}

\noindent \textbf{Configuration for destination MAC address verification}
\begin{enumerate}[resume]
	\item The HASH table entry for destination MAC address from the~request and the~Filtering~ID read previously from the~VLAN~table is searched.
	\item If the~entry is not found in the~HASH~table, the~frame associated with the~request is supposed to be broadcasted, provided that broadcasting unrecognized frames for the specific port ID is enabled.
	\item If the~entry is found and has the~\textit{drop when destination} flag set, the~frame associated with the~request is supposed to be dropped.
	\item If frame associated with the request has not local-link destination MAC address, \textit{pass~all} parameter is unset for the source port, the frame is supposed to be dropped.
\end{enumerate}

\noindent \textbf{Topology resolution configuration verification}
\begin{enumerate}[resume]
	\item If the~topology resolution configuration verification is enabled and it indicates a drop flag for given MAC addresses and the~Filtering~ID, the~frame associated with the~request is supposed to be dropped.
\end{enumerate}

\noindent \textbf{Final forwarding decision}
\begin{enumerate}[resume]
	\item If the~frame associated with the~request is intended to be dropped, the forwarding engine response contains a port ID parameter equal to the source port ID from the request, a~\textit{drop}~flag set and the rest of the parameters is cleared. The~data set of the~forwarding engine response is specified in \ref{t:response}.
	\item If the~frame associated with the~request is intended to be broadcasted, the forwarding engine response contains a port ID parameter equal to the source port ID from the request, a~\textit{drop}~flag is unset and the output port mask is restricted only by a~port mask configured for the given VLAN~ID and a~port mask resulting from a~topology resolution configuration. The~data set of the~forwarding engine response is specified in \ref{t:response}.
	\item Otherwise, the output port mask is additionally restricted by a~port mask read from the~MAC table entry found for destination MAC address.
	\item The final priority of the frame associated with the~request is evaluated on the basis of priority assigned to the given VLAN~ID, combination of Filtering~ID and source MAC address, combination of Filtering~ID and destination MAC address.  
\end{enumerate}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.92]{pic/forwarding-decision.pdf}
	\caption{The forwarding decision algorithm.}
	\label{fig:forwarding-decision}
\end{figure}

\section{Forwarding Engine Architecture}

\noindent According to project assumptions, the~switch is supposed to handle up to 360~MPPS, which is the~result of supporting up to 24~ports with a~10~Gb/s link and working in a full-duplex mode. Consequently, the~forwarding engine must be ready to get new requests every 2.8~ns to ensure switch determinism. Additionally, the~forwarding decision must be provided within 67.2~ns, which is the~time it takes to receive a minimum-size frame (including interpacket gap).

%fully pipelined (requests received every clock cycle)
%24 ports per single engine (2.8ns) > 358MHz
%12 ports per single engine (5.6ns) > 179MHz / 2 forwarding engines
%
%requests received every two clock cycles
%12 ports per single engine (5.6ns) > 358MHz / 2 forwarding engines
% 6 ports per single engine (11.2ns) > 179MHz / 4 forwarding engines

Even with a fully pipelined dataflow architecture and accepting a new request in every clock cycle, the forwarding engine must work with at least 358MHz frequency.
Although that frequency is theoretically accessible for the Xilinx Zynq Ultrascale+ XCZU17EG-1FFVC1760E device~\cite{ultrascale-datasheet}, it may appear that timing constraints are not met when the~overall White Rabbit Switch firmware is implemented. It may also be restricted by the Xilinx IP Cores performance.

Under that circumstances, forwarding engines, including memory tables, must be multiplicated to guarantee a~\mbox{wire-speed} throughput. The number of required forwarding engine instances is proportional to the number of clock cycles when the forwarding engine is not ready to accept subsequent request. Therefore, the processing must be as pipelined as possible. Especially, the necessity to read the same memory block twice within processing a single request must be avoided -- it is more effective to multiply a MAC table only than to multiply a whole forwarding engine\footnote{Both source and destination MAC addresses are searched in a~MAC table within a single request processing. However, only a single memory port is available for lookup - the second one is used for updates from processing system. The searching could be performed sequentially using the same memory table, but the~table is multiplied to achieve higher throughput.}.

Additionally, it should be noticed that a final forwarding decision also depends on the~TRU module response. The~TRU module can accept requests in every clock cycle and provides a~response within two clock cycles. However, it may be necessary to multiplicate the~TRU module along with the TRU memory table.

\noindent The flowchart depicted in figure \ref{fig:forwarding-decision} splits the processing into subsequent stages:
\begin{itemize}
	\item initial configuration verification,
	\item VLAN configuration verification,
	\item configuration for source MAC address verification,
	\item configuration for destination MAC address verification,
	\item topology resolution configuration verification,
	\item final forwarding decision.
\end{itemize}

The VLAN configuration verification must precede further steps as the Filtering ID read from the VLAN table is, along with the MAC address, part of a~MAC table selector. Nevertheless, searching a~MAC table for the entries associated with source and destination MAC addresses, as well as topology resolution configuration verification, can be made in parallel. The final forwarding response is generated if all previous steps are done. 

The flowchart illustrates that the decision to drop the frame associated with the request can be made at each stage of the processing. Nonetheless, due to the pipelined architecture and the fact that responses must follow the order of requests, even if a response is known in advance, further processing is not omitted.
The forwarding engine interfaces remain unchanged and are defined in the~appendix \ref{forwarding-engine-configuration}.

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.67]{pic/wr-switch-new.pdf}
	\caption{The developed White Rabbit Switch forwarding engine architecture.}
	\label{fig:wr-switch-new}
\end{figure}

\subsection{VTAB Reader module}

\noindent The~VTAB reader module reads a~VLAN table entry corresponding to the VLAN ID from the~request. Its parameter is a~VLAN table read latency given in clock cycles. On the basis of the~forwarding engine request and the~read VLAN table entry the module forwards:
\begin{itemize}
	\item part of the request data set needed to make a~final forwarding decision (\texttt{M\_axis\_req}),
	\item part of the VLAN table entry needed to make a~final forwarding decision (\texttt{M\_axis\_vtab}),
	\item Filtering~ID and source MAC address for HASH table lookup (\texttt{M\_axis\_htab\_src}),
	\item Filtering~ID and destination MAC address for HASH table lookup (\texttt{M\_axis\_htab\_dst}),
	\item learning FIFO data set (\texttt{M\_axis\_learning\_fifo}),
	\item TRU module request (\texttt{M\_axis\_tru}).
\end{itemize}

\subsection{HTAB Lookup module}

\noindent The HTAB Lookup module is responsible for performing HASH table lookup for a given Filtering~ID and MAC address, according to the HASH table structure presented in section \ref{fib-implementation}. Its parameter is a~HASH table read latency given in clock cycles. The module forwards a~found HASH table entry or sets a~not found flag on an~output signal \texttt{tuser}.

\subsection{MTAB Reader module}

\noindent The~MTAB reader module reads a~MAC table entry from the~address contained in a~found HASH table entry. Its parameter is a~MAC table read latency given in clock cycles and a~source/destination flag indicating which fields from a~read MAC table entry should be taken into account\footnote{For instance, \texttt{drop\_when\_source} or \texttt{drop\_when\_destination}.}. The module forwards a data set needed to make a~final forwarding decision, including a~drop flag when a~HASH table entry has not been found or when the~FID and the~MAC address from a~HASH table entry are non consistent with the~FID and the~MAC address from a~MAC table entry.

\subsection{Forwarding Decision Maker module}

\noindent The Forwarding Decision Maker module is responsible for making a~final forwarding engine decision on the basis of:
\begin{itemize}
	\item forwarding engine configuration,
	\item forwarding engine request data set (\texttt{S\_axis\_req}),
	\item VLAN table entry (\texttt{S\_axis\_vtab}),
	\item MAC table entry for source MAC address or not found flag (\texttt{S\_axis\_mtab\_src}),
	\item MAC table entry for destination MAC address or not found flag (\texttt{S\_axis\_mtab\_dst}),
	\item TRU module response, if TRU processing enabled (\texttt{S\_axis\_tru}).
\end{itemize}
It is assumed that all enumerated data sets are received in the~same clock cycle.
The module implements the algorithm described in section \ref{forwarding-decision-algorithm} and determines forwarding engine response components: drop flag, output port mask and frame priority. Besides, if a~HASH table entry for a~source MAC address has not been found and a~learning process is enabled, it forwards a~learning FIFO data set (received on \texttt{S\_axis\_learning\_fifo} interface) to a~learning FIFO.

\section{Multiple Forwarding Engines}

TODO

\section{Communication with the Zynq Processing System}

TODO

%The CPU
%port is needed for device management (register
%and table access) and control plane functions
%(MIB, BPDU, etc.).

% Shadow musi byc wszystkiego w PS bo nie ma mozliwosc bezposredneigo odczytu z PL (szkoda cylki), PS tylko pisze do PL, na podstawie rob aging

% infrastruktura AXI - ile zasobow

%Zalozenia co do softu zarzadzajacego tabelami:
	 % mozna zrezygnowac z przetrzymywania FID i MAC rowniez w MAC table pod warunkiem, ze przy usuwaniu / nadpisywaniu wpisu soft najpierw ustawi not valid w HASH table, potem nadpisze/usunie zawartosc MAC table i dodatkowo, ze odstep pomiedzy tymi operacjami ze strony softu bedzie wiekszy niz odstep pomiedzy odczytem HTAB i MTAB w hardwarze (zeby nie bylo opcji, ze w forwarding engine odczytany bedzie slot HTAB z flaga valid a potem slot MTAB, ktorego zawartosc nie odpowiada odczytanemu wczesniej slotowi HTAB).
	% moze byc i d-ary hashing, z punktu widzenia logiki bedzie to samo co d-left? (te same funkcje hashujace?) - ale potrzebne shadow memory do update'ow

% TODO zapisywanie konfiguracji do rejestr�w

\section{Forwarding Engine Alternative Architectures}

\noindent The White Rabbit Switch firmware has not been adapted to the~increased number of ports and their speed yet. The developed forwarding engine is the~first module which addresses the~change in the~required performance. Therefore, its submodules are as modular as possible and alternative engine's architectures are presented. Such approach allows for relatively convenient project modifications when the~frequency or available resources will differ from the~assumptions.

%Modular design through standardized interfaces.
%Providing a modular design is essential to futureproofing implementations and ensuring that additional
%functionality can be easily added at later stages.

% Konkretne wymiary tabeli, odniesienie do \cite{ultrascale-memory}

%- uproszczona obsluga AXIS - zakladamy, ze zawsze jest tready, co upraszcza logike, ale zmniejsza elastycznosc modulu



TODO