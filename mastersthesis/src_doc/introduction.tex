\noindent The~White Rabbit is the~technology introduced in 2006 as the~collaboration of the~\acrfull{cern} and GSI Helmholtz Centre for Heavy Ion Research~\cite{space} to provide sub-nanosecond accuracy, picoseconds precision of synchronisation and deterministic data transfer in Ethernet-based networks. The~strict network characteristics were specified to be able to coordinate thousands of devices working at the~\acrshort{cern}'s accelerator complex, and at the~time of defining the~White Rabbit’s concept, no networking standard could meet the~requirements~\cite{lipinski}. 

Even though the~control and timing systems working then at \acrshort{cern} were fully functional, they were custom-made. As a consequence, they suffered from a lack of compatibility with other control systems, complicated maintenance, scalability problems and architectural limitations. For instance, the~\acrfull{gmt} System used to control the~\acrfull{lhc}, the~world's largest and highest-energy particle collider, was unidirectional, based on obsolete RS-422 technology and had limited bandwidth. The~\acrshort{wr} technology was meant to address the~foregoing issues and ensure flexibility, interoperability and broad commercial support missing in the~previous custom designs.

Backward compatibility with the~existing, well-established \acrshort{ieee} standards and only extending them if needed is one of the~White Rabbit's  fundamentals. The~White Rabbit Network is a Bridged Local Area Network with VLANs (\acrshort{ieee}~802.1Q). Its switches and nodes are interconnected using Ethernet (\acrshort{ieee}~802.3), and the~Precision Time Protocol (\acrshort{ieee}~1588-2008) was adapted to synchronise them.

As the~base of versatile, future-proof control and timing systems, which was initially designed for usage in critical \acrshort{cern}'s experiments, the~White Rabbit fulfils numerous specific assumptions. The~devices in such real-time distributed systems must be coordinated and execute operations in a precise and timely way. Hence the~common notion of time and frequency is distributed through a network in a hierarchical manner from the~reference to all the~nodes. The~timing requires synchronisation with sub-ns accuracy\footnote{Accuracy is defined as the~absolute value of the~average time error between the~reference and the~device synchronised to this reference.} and the~precision\footnote{Precision is defined as the~standard deviation of the~time error.} better than 50~ps~\cite{lipinski}.

The~vast area of the~\acrshort{lhc} makes it necessary to connect and synchronise thousands of devices spread over tens of square kilometres, with a maximum distance of 10~km between each other. Despite the~distance, the~maximum time interval from scheduling the~task in a controller to its completion in a receiver, called the~upper-bound latency, is defined as 1~ms. Moreover, the~latency through a single switch is required to be at most 10~$\mu$s. Dedicated control messages triggering simultaneous processes in sparsely distributed accelerator devices are the~critical network traffic. They are supposed to be delivered from the~central master device to the~number of receiving devices in the~expected time, despite other control traffic flow. Robust delivery of high priority messages through a predictable path along with bi-directional flows of other, non-critical traffic is crucial to guarantee the~system's determinism. 

The~requirements for the~White Rabbit based systems are summarised in table \ref{t:wr-system-requirements}. Due to the~possibility of severe consequences in case of a critical system failure, like the~expensive downtime or damage of the~devices, none of the~system components should be enhanced without the~overall understanding of all its pieces and being aware of the~essential network assumptions.

\begin{table}[h!]
\begin{threeparttable}
	\centering
	\caption{CERN requirements for the~White Rabbit based systems.}
	\vspace{1mm}
	\label{t:wr-system-requirements}	
	\begin{tabular}{||>{\centering}m{0.6\textwidth} | 
			        >{\centering\arraybackslash}m{0.3\textwidth}||} 
		\hline
		\textbf{requirement} & \textbf{value(s)} \\ \hline\hline
		network size: maximum distance and number of receivers & 10 km \& 2000 \\ \hline
		accuracy & sub-ns \\ \hline
		precision & sub-50ps \\ \hline
		control message size & 1200-6000 B \\ \hline
		maximum number of control messages lost per year & 1 \\ \hline
		upper-bound latency through a network \newline and a single switch & 1 ms \& 10 $\mu$s \\ \hline
	\end{tabular}
\begin{tablenotes}
	\small
	\item \textit{Source:} \cite[table 1.1]{lipinski}.
\end{tablenotes}
\end{threeparttable}
\end{table}

The~White Rabbit Project is a long-term, multi-company and multinational cooperation~\cite{wr-wiki-collab}. Its foundations were laid by the~work of Tomasz Wlostowski~\cite{wlostowski}, who provided the~basis of~the~White Rabbit Switch implementation and succeeded in receiving the~anticipated level of precision and accuracy of synchronisation in an Ethernet-based network. 

Later on, Grzegorz Daniluk developed~\cite{daniluk} the~White Rabbit \acrshort{ptp} Core -- \acrshort{hdl} module handling the~sub-nanosecond synchronisation -- which is, in turn, the~key element of the~White Rabbit Node gateware. The~form of a complete, standalone \acrshort{ip} Core facilitates the~use of the~module even in existing devices.

% NOTE
% A deterministic system is predictable: it provides calculable and consistent characteristics of operation that are required by the application, e.g. latency of data transmission and its rate of delivery, throughput.
%A reliable system performs its required functions under stated conditions for a specified period of time. The components of reliability are identified as performance and fault tolerance.

One of the~most significant project's milestones were the~White Rabbit Switch architecture's enhancements and reference network design proposed by Maciej Lipinski~\cite{lipinski}. Provided solutions, including appropriate network topology and redundancy mechanisms, allowed increasing the~WR network reliability and ensuring deterministic data transfer of critical traffic also in case of network reconfiguration, thus fulfilling the~\acrshort{cern} control and timing system requirements. 

The~White Rabbit Switch hardware, firmware and software are being developed in compliance with the~open-source paradigm to avoid vendor lock-in and are publicly available in the~\acrshort{cern}'s Open Hardware Repository. A fully open design and standard compatibility are the~relevant factors contributing to the~project's success. The~long-lasting joint efforts of the~White Rabbit team has resulted in the~completion of the~WR Standardization process. Since June 16, 2020, the~White Rabbit technology is a part of \acrshort{ieee}~1588-2019 -- \textit{Standard for a Precision Clock Synchronization Protocol for Networked Measurement and Control Systems}~\cite{wr-standard}. Incorporating the~\acrshort{wr} addition increases \acrshort{ptp}’s synchronisation performance by a few orders of magnitude.

What is more, the~\acrshort{wr} technology, firstly deployed in experiments related to high energy physics, has expanded far beyond its initial field of application. Nowadays, it is used worldwide in numerous industries, including telecommunications, financial markets and the~space industry~\cite{wr-wiki-users}.