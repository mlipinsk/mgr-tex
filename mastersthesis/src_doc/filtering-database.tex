\section{Filtering Database Structure}

\noindent In the previous chapter the Filtering Database implementation criteria were summarized and the number of FPGA-based high performance implementations have been proposed. Their review along with the most important design assumptions, enumerated below, was the basis for the selection of the target Filtering Database structure.
\begin{itemize}
	\item The Filtering Database is supposed to be stored in embedded, high bandwidth memory (BRAM and/or Ultra-RAM) and around 14 Mb is available. High memory utilization is one of the most important Filtering Database structure aspects.
	\item Network configuration is not supposed to be modified frequently. Thus, the lookups to the insertions ratio is high and most lookups are supposed to be successful.
	\item The forwarding engine must be able to perform lookup for every single request, even if the maximum number of packets per second is received on each of the switch ports. Therefore, deterministic worst-case lookup time is critical.
	\item The White Rabbit Switch is a~VLAN-aware device and supports shared VLAN learning. In~terms~of implementation it implicates separate memory tables for mapping MAC addresses and FIDs to device ports and for VLAN configuration.
	\item Current Filtering Database entries are defined in \cite{wrs-hdl-sw}. For backward compatibility, changes will be introduced only if necessary. 
	\item Insertion algorithm should not require using shadow memory - probably there is no enough memory resources for that.
\end{itemize} 
\ \\
\noindent Taking the above considerations into account, primary conclusions may be drawn:
\begin{itemize}
	\item A~single VLAN configuration entry has 8~B (as specified in appendix~\ref{filtering-database-entries}). The 12-bit VLAN~ID has 4096 possible values, which correspond to VLAN table entries' indexes. Therefore, the~configuration for the~specific VLAN~ID may be read directly, and no search mechanism is needed. Thus, a~\acrlong{vtab} size must be 32~KB (4K entries).
	\item A~single MAC address configuration entry has 16~B (as specified in appendix~\ref{filtering-database-entries}). Thus, a~\acrlong{mtab} size must be at least 160~KB (up to 10K entries).
	\item The hash table structure is bound to store MAC table and d-left hashing schema is chosen to reduce the probability of unresolved hash collisions. Such a solution fulfills conditions concerning high memory utilization and deterministic worst-case lookup time. Additionally, it allows for taking advantage of possibility of parallel access to embedded memory blocks. 
	\item Each sub-table must be a~separate memory block to read them in parallel. 
\end{itemize}
\ \\
\noindent It is beneficial in terms of memory utilization to separate a hash table containing selectors only from a MAC table containing selector assigned configuration. Then a MAC table contains only 10K entries and a HASH table contains $S > 10K$ entries to obtain appropriately low collision probability. Such a separation may be introduced at the cost of additional forwarding process latency. With FID and MAC address as a selector, an MAC table entry specified in the~appendix~\ref{filtering-database-entries} and up to 10K active nodes in a network, the separation is beneficial if $S > 12.4K$. However, the advantage is smaller if a MAC table is supposed to store selector as well to guarantee an atomic read operation. Then, the separation is beneficial if $S > 23.9K$.
% NOTE
% selector in MTAB:
% S * 72b + 10K * 124b < S * 124b
% S  > 23.9K 
% no selector in MTAB:
% S * 72b + 10K * 64b < S * 124b
% S > 12.4K

The alternative Filtering Database structures are depicted in figures \ref{fig:filtering-database-structure-separate} and \ref{fig:filtering-database-structure}. The aspects that cannot be predicted and should be emulated with the use of the~reference software design comprise: the number of sub-tables -- $d$, hash functions used -- $h_{1}, h_{2}, ..., h_{d}$, the number of buckets in a~single sub-table -- $m$, the number of slots per bucket -- $s$ and, thus, the overall number of slots in a hash table -- $S = d \cdot m \cdot s$.

\begin{figure}[H]
	\centering
	\includegraphics[scale=1]{pic/filtering-database-structure-separate.pdf}
	\caption{The Filtering Database Structure - separate HTAB and MTAB.}
	\label{fig:filtering-database-structure-separate}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[scale=1]{pic/filtering-database-structure.pdf}
	\caption{The Filtering Database Structure - MTAB as a hash table.}
	\label{fig:filtering-database-structure}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% NOTE
% \cite{ultrascale-memory}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% BRAM
% Single block up to 36 Kbits, two write and two read ports, one 36Kb / two 18Kb.

% 36Kb:        18Kb:

% 32K x 1b     
% 16K x 2b     16K x 1b
%  8K x 4b      8K x 2b
%  4K x 9b      4K x 4b
%  2K x 18b     2K x 9b
%  1K x 36b     1K x 18b (only if True Dual Port RAM)
% 512 x 72b    512 x 36b (only if Simple Dual Port RAM)

% HASH table : 1/2/4K x 72b (FID + MAC + pointer to one of 10K addresses of MAC table)
% MAC table: 10K x 124b (min 118b)
% VLAN table:  4K x 40b (min 38b)

% 1/3 * 600 BRAM = 200 BRAM = 14 Mb?
% BRAM / UltraRAM

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% URAM
% Single block 288Kbits, dual port, 4K x 72b

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Hash Functions}

\noindent On the whole, the choice of a~hash function for a~hash table should be based on two most important criteria -- uniformity and low computational cost~\cite{aoc}. 

Uniform hash function provides mapping the expected keys as evenly as possible over its output range. In other words, for the~$n$-bit hash function the~probability for each possible result should be roughly the same -- $1/2^n$. Using hash functions with relatively uniform distribution is the~measure to reduce the~probability of hash collisions.

The uniformity of the distribution of hash function output values may be evaluated with chi-squared test and the result should be in range (0.95, 1.05) to indicate that a~hash function is uniform:
\begin{equation}
u = \frac{\displaystyle\sum^{m-1}_{j=0}(b_{j})(b_{j}+1)/2}{(n/2m)(n+2m-1)}
\end{equation}
where $m$ is the number of all possible hash indexes, $n$ is the number of hashed keys, and $b_{j}$ is the number of items assigned to hash index $j$.

Since the~IETF document \textit{Benchmarking Methodology for LAN Switching Devices} (RFC 2889) recommends using sequential addresses for lookup table capacity tests \cite{rfc2889}, it is observed that hash functions used in commercially available switches are optimized for storing sequential keys rather than random ones \cite{effect}. Although this approach is beneficial in a~marketing context, the~use of solely sequential addresses in real-life networks is quite unlikely. Therefore the~experiments with the~use of generated random addresses or data~from statistical analysis of Ethernet traffic are more reliable and valuable.

Hash function computational cost involves required logic resources for hardware implementation and number of clock cycles needed to perform a~computation. For instance, for purposes of hash tables all cryptographic functions performance is unacceptable. The optimal solution for network applications in terms of computational cost along with the uniformity, and amenable to hardware implementation, are cyclic redundancy checks~\cite{crc, hash-function}.

\section{Hash Collisions Simulations}


%In summary, you use chi2_contingency when you don't know the underlying distribution but you want to test whether two (or more) groups have the same distribution. You use chisquare when you have a distribution in mind and you want to test whether a group matches that distribution.


TODO

%Block RAM

%BRAM - pami��szybkiego dost�pu implementowana w logice programowalnej (Block RAM).
%Od strony FPGA jest to pami�� swobodnego dost�pu, zapis oraz odczyt trwa jeden cykl zegara (4-5 ns).
%Jest widziana przez system operacyjny w jego mapie pami�ci, fizyczny dost�p do niej jest zapewniany przez interfejs AXI4.
%Wykorzystywna jest m.in. do implementacji struktur danych, do kt�rych uk�ad FPGA cz�sto si�ga i wykonuje na przemian
%zar�wno zapisy jak i odczyty.
%W niekt�rych przypadkach pami�� BRAM mo�e zosta� zast�piona pami�ci� URAM (Ultra RAM), je�eli logika programowalna w wybranym
%uk�adzie posiada takie zasoby. Pami�� URAM mo�na traktowa� w uproszczeniu jak dwuportow� pami�� BRAM o wi�kszej granulacji (8 razy), o sta�ej 
%szeroko�ci portu (72 bity) oraz z pojedyncznym zegarem dla obu port�w.
%%
%Block RAMs (or BRAM) stands for Block Random Access Memory. Block RAMs are used for storing large amounts of data inside of your FPGA. 
%A Block RAM (sometimes called embedded memory, or Embedded Block RAM (EBR)), is a~discrete part of an FPGA, meaning there are only so many of them available on the chip. Each FPGA has a~different amount, so depending on your application you may need more or less Block RAM. Knowing how much you will need gets easier as you become a~better Digital Designer. As I said before, it's used to store "large" amounts of data inside of your FPGA. It's also possible to store data outside of your FPGA, but that would be done with a~device like an SRAM, DRAM, EPROM, SD Card, etc.
%
%Block RAM (BRAM) is a~type of random access memory embedded throughout an FPGA for data storage.
%You can use BRAM to accomplish the following tasks:
%
%Transfer data between multiple clock domains by using local FIFOs
%Transfer data between an FPGA target and a~host processor by using a~DMA FIFO
%Transfer data between FPGA targets by using a~peer-to-peer FIFO
%Store large data sets on an FPGA target more efficiently than RAM built from look-up tables


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%\cite Maximizing the Throughput of Hash Tables in Network Devices with Combined SRAM/DRAM Memory

%\subsubsection{Parallel d-Pipeline}
%\label{parallel-d-pipeline}

% parallel d-pipeline
%The continuous increase in speed and
%complexity of networking devices creates a~need for higher throughput exact
%matching in many applications. In this paper, a~new Cuckoo hashing
%implementation named parallel d-pipeline is proposed to increase throughput. The
%scheme presented is targeted to implementations in which the tables are accessed
%in parallel.


%\cite{multiple-hash-ip-lookup}
%a natural goal is to ensure that the number of
%items that fall in a~bucket corresponds to the capacity of a~single
%cache line, so that each hash bucket corresponds to a~cache line
%of memory. This ensures that each level examined during the
%binary search only requires a~single memory access

%\cite{parallel-d-pipeline}

% TODO czy technika parallel d-pipeline nadaje sie tylko do cuckoo hashing czy tez do innych przypadkow (jakich)?

%Ciekawy i przydatny (pozwalajacy zwiekszyc przepustowosc) algorytm do wykorzystania w sytuacji wielu odczytow 
%z roznych obszarow pamieci w ramach przetwarzania jednego zapytania.
%
%In ASIC/FPGA implementations and for small-medium tables,
%an embedded memory per table can be easily used so that tables
%are accessed in parallel to further speed up access.
%Then the worst case access time is fixed and given by just one memory access.
%
%However, as discussed in the following, in many
%networking applications, most searches are successful.

%As mentioned before, the d tables can be stored in one or several
%memories. In the first case, tables are accessed serially and in the
%second case in parallel. For the serial access, the worst case access
%time occurs when the element is not stored in the tables or it is
%found in the last table. In both cases, d memory accesses are
%needed. The average number of accesses will be lower and
%depends on the table occupancy and on the strategy used for insertion. As with d-left hashing, ordered insertion provides a~lower
%average access time as items are concentrated on the first tables
%and therefore found faster.

%In the parallel case, there are d memories such that each table is
%stored in a~different memory and search operations can be done by
%accessing all memories in parallel. In that case, all searches require
%d accesses and are completed in one memory cycle. Alternatively,
%search operations can be pipelined to reduce the number of memory accesses. In the pipeline architecture, a~search operation
%accesses each of the memories sequentially such that when the current operation moves to access the second memory, the next search
%operation can access the first memory. Therefore the performance
%in terms of throughput is the same as that of parallel searches.
%However, in the pipeline architecture once a~search is successful,
%the rest of the memories do not have to be accessed. 

%Therefore, the
%average number of accesses is reduced to that of the serial case.
%Due to the pipeline, all the search operations are completed in d
%memory cycles.

% zestawienie rysunkow:
% serial
% parallel
% parallel pipeline
% parallel d-pipeline
% + opis do nich o co chodzi z kolorami kwadratow

% parallel pipeline implementation
%In this architecture, an incoming element is searched in table one in the first cycle. If the element is not found, then it is
%searched in table two and so on. As soon as the element is found,
%no further searches are done and the element is just propagated
%through the pipeline until it reaches the output.

% parallel d-pipeline implementation
%The proposed technique to optimize the throughput of Cuckoo
%hashing implementations is based on a~simple observation: in the
%parallel pipeline implementation, the tables are not accessed in all
%cycles. For example, in a~parallel pipeline implementation if an element is found in Table 1, then on the following cycles Tables 2,3,. . .,
%d are not accessed.
%Therefore, throughput can potentially be increased with implementations that make use of all the tables in all the cycles.

% opis jak dokladnie dziala algorytm

%For the traditional pipeline implementation shown in Fig. 1, the
%throughput is constant and equals one element per cycle. This is
%not the case for the new parallel d-pipeline scheme (or for the serial
%scheme) in which throughput depends on several factors. The first
%one is the percentage of the matching operations that are successful. This is because an operation that fails requires accesses to all
%d tables and leaves no room for improvement. On the other hand, a
%successful operation may require less than d accesses leaving some
%cycles to be used by other operations. The second factor is whether
%ordered or random insertion is used in the Cuckoo hashing. Finally
%and related to the second factor, the occupancy of the tables also
%affects the throughput.

% zzalozeniem ze table occupancy is close to 100 percent i sa wykorzystywane 4 tabele cuckoo hashing
%Since in the proposed
%implementation the four tables are accessed in all cycles the average throughput per cycle will be approximately 4/2.5 ? 1.6. This
%means a~60 percent increase over the traditional pipeline implementation that provides a~throughput of 1.0. 
%
%The traditional systems (both standard and pipeline) have a
%throughput of 1.0 and therefore are able to process up to
%156.25 million of packets per seconds. Supposing a~minimum
%packet size of 40 bytes the maximum throughput sustainable
%under worst case packet size conditions is 50 Gbps. Instead, since
%the proposed parallel d-pipeline can be clocked at the same frequency as the traditional parallel pipeline implementation, but
%processes an average number of 1.6 packets per cycle, the
%maximum throughput can be estimated as 80 Gbps. Therefore, for
%a given FPGA technology, the packet throughput can be increased
%significantly. This clearly illustrates the benefits of the proposed
%scheme. The main disadvantage of the scheme is that it requires
%some additional FPGA resources.

%In this paper, an architecture to increase the throughput of Cuckoo
%hashing implementations has been proposed and evaluated. The
%scheme is targeted to high-speed parallel implementations and
%modifies the existing pipeline architecture to ensure that all tables
%are accessed in all cycles. This is done by introducing one pipeline
%per table such that elements can enter the system at any given
%table. This enables a~more efficient use of the tables and therefore
%an increase in throughput.

%The overheads in the FPGA implementation are less than
%5 percent in the case study. This increase in FPGA resource usage
%is clearly offset by the gains in throughput.

%The proposed architecture could also be potentially used to
%speed up parallel implementations of other multiple hash schemes
%such as d-left hashing and Bloom filters [18]. This can be an attractive topic for future work.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% wiki
%https://en.wikipedia.org/wiki/Hash_array_mapped_trie
%https://en.wikipedia.org/wiki/Hash_tree_(persistent_data_structure)



%\cite{ultra-fast}
%To reduce the probability of collision it is possible to use adaptive hashing combined with the proposed method. One of
%the parallel tables should have a~changeable law of the hash calculating. When the collision appears the hash calculating
%law for one of the parallel tables should be replaced with another one. This allows to significantly reducing the collision
%probability without increasing of memory usage and optimizing searching time.

%biblio: Technologies and Building Blocks for
%Fast Packet Forwarding

%Over the~past few years, significant progress
%has been made in the~development of forwarding algorithms and implementations. Most techniques, however, address only a~subset of the
%above parameters (speed, size, and update performance) [4]. Only recently have algorithms
%been proposed that address the~classification
%and forwarding problem in a~generic way based
%on worst-case assumptions and no longer rely on
%special properties of the~forwarding and classification tables and rules [5]. Related work at IBM
%Research [6] has concentrated on finding
%approaches that introduce pipelining by segregation of the~forwarding key in suitable bit fields
%and then dynamically deciding whether the
%longest-prefix matched (LPM) lookup of a~field
%is done as a~bit test or a~table lookup, depending
%on whether the~forwarding table is locally sparsely filled. This approach tends to hold the~information on a~specific prefix localized and not
%compressed, allowing fast updates. Lookup times
%equal a~single memory access cycle and the~table
%size scales better than O(P), with P being the
%number of prefixes in the~forwarding table. Classification can be decomposed in a~similar fashion, allowing parallel range searches. The results
%of the~range searches are combined into a~variable-sized prefix, which can be resolved through
%the above LPM lookup to yield the~final classification result.


%Parallel hashing, similarly to adaptive hashing methods, bases on the power of using multiple hash functions to reduce the probability of collisions. The concept is used in d-random Hashing, d-left Hashing or Cuckoo Hashing, but there are known implementations 



%\cite{aoc} str 545

% TODO rozroznic zlozonosc time complexity dla softu zarzadzajacego zawartoscia tablicy a~hardwarem, ktory z niej korzysta

% \cite{parallel-d-pipeline}
%When external
%memories are used, all tables are commonly stored in the same
%memory device and are accessed serially. The use of a~unique
%external memory device to store all the hash tables is due to the pin
%limitations of network hardware devices which pin count can
%already exceed 1,000 pins [10]. 

% \cite{parallel-d-pipeline}
%In most applications of Cuckoo hashing in networking, the percentage of search operations that are successful is close to 100 percent. This is because in these applications, when an item not
%present in the table is accessed, the system must add the item to
%the hash table. 
%network switches, in which the table
%is updated when new flows arrive or the link state changes 

%The~methods presented below sometimes are combined with an~overflow buffer, which
%stores the~entries that would be otherwise rejected due to the~lack of free space in the~corresponding hash table structures. Such a~buffer holds entries regardless of their assigned hash value and most often is searched sequentially \cite{effect}. 
%
%It should be emphasized that the~problem of collisions and decreased lookup table actual capacity concerns only hash-based solutions. Tables implemented in content-addressable memory or as a~binary search tree are capable of storing random as well as sequential addresses up to the table size~\cite{effect}.

%\cite{optimizing-lookup}
%generalnie nie ma tu propozycji �adnej architektury konkretnej, opisane jest narz�dzie, kt�re pozwala uwsp�lni� i zgeneralizowa� problem r�nych mechanizm�w lookup i dobra� rozwi�zanie
%With the advancements in FPGA technology, achieving
%40+ Gbps per pipeline packet forwarding rates using existing
%search techniques (e.g. hash, CAM, tree, trie) has become
%commonplace [7], [10], [2], [3], [13].
% biblio 2,3,7,10,13 stad!!!

%\cite{cuckoo}
%porownanie na wykresach: cuckoo, 2-way, separate, linear probing

% - power
% - resource utilization
% - latency
% - limitations
% - performance
% update time
% lookup time
% average lookup time
% worst-case lookup time
% insert time
% delete time
% memory used
% space
% memory bandwith required 

% bloom - time - $O(k)$
%\cite{hash-ip-lookup-scalable-bloom} % tabelki, wykresy z por�wnaniem
%\cite{optimizing-lookup}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% This section briefly explains
% congruency
% By necessity
% For that reason 
% future-proof - d�ugo nie wychodz�cy z u�ycia
% notable achievement was the concept of
% miscellaneous - rozmaity, r�norodny, pomieszany
% Problems notwithstanding,
